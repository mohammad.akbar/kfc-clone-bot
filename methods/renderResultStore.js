function renderResultStore(ctx){
    var stores = [...ctx.payload.result.result];
    var resto = "";
    
    stores.forEach(store => {
        resto += store.name + "\n";
        resto += store.address + "\n";
        resto += store.noTelp + "\n";
        resto += "\n"
    })

    var renderedText = {
        type: "text",
        content: resto
    };

    return renderedText;
}