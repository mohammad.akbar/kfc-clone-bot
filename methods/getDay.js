function getDay(ctx){
    var today = new Date()
    var currDay = today.getDay()
    var dayLabel = ['Minggu','Senin','Selasa','Rabu','Kamis',"Jum'at",'Sabtu']
    ctx.context.day = dayLabel[currDay]
    return ctx
}