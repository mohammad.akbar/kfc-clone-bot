function getTimeByTxt(ctx){
    var today = new Date()
    var curHr = today.getHours() + 7

    if (curHr < 10) {
        ctx.context.time = 'pagi'
    } else if (curHr < 15) {
        ctx.context.time = 'siang'
    } else if (curHr < 18) {
        ctx.context.time = 'sore'
    } else {
        ctx.context.time = 'malam'
    }
    
    return ctx
}