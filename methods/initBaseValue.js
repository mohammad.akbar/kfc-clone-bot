function initBaseValue(ctx){
    ctx.data.baseValue = {nol:0,satu:1,dua:2,tiga:3}
    // ctx.data.baseValue = [0,1,2,3]
    ctx.data.scoreValue = 0
    return ctx
}

function calculateScoreGAD(ctx){ //
    let intent = ctx.context.intent 
    ctx.data.scoreValue = ctx.data.scoreValue + ctx.data.baseValue[intent]
    return ctx
}

function calculateScorePHQ(ctx){ //
    let intent = ctx.context.intent 
    ctx.data.scoreValue = ctx.data.scoreValue + ctx.data.baseValue[intent]
    return ctx
}