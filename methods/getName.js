function getName(ctx) {
    ctx.context.name = "Akbar";
    if (ctx.metadata && ctx.metadata.channelType && ctx.metadata.channelType == "telegram") {
        ctx.context.name = ctx.metadata.telegramSenderName;
    }

    return ctx;
}
